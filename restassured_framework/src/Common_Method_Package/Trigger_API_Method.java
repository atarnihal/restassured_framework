package Common_Method_Package;

import static io.restassured.RestAssured.given;

import Request_Repository.Post_Request_Repository;

public class Trigger_API_Method extends Post_Request_Repository {

	public static int extract_status_code(String req_Body, String URL) {
		int StatusCode = given().header("Content-Type", "application/json").body(req_Body).when().post(URL).then()
				.extract().statusCode();

		return StatusCode;
	}

	public static String extract_Response_Body(String req_Body, String URL) {
		String responseBody = given().header("content-Type", "application/json").body(req_Body).when().post(URL).then()
				.extract().response().asString();
		return responseBody;

	}

}
