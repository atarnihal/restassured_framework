package Common_Method_Package;

import static io.restassured.RestAssured.given;
import Request_Repository.Post_Request_Repository;

public class Trigger_Delete_API_Method extends Post_Request_Repository {
	public static int extract_SC_Delete(String URL) {

		int statusCode = given().header("Content-Type", "application/json").when().delete(URL).then().extract()
				.statusCode();
		return statusCode;
	}

}
