package Common_Method_Package;

import static io.restassured.RestAssured.given;

import Request_Repository.End_Points;

public class Trigger_Get_API_Method extends End_Points {

	public static int extract_Status_Code(String URL) {
		int Status_Code = given().header("Content-Type", "application/json").when().get(URL).then().extract()
				.statusCode();

		return Status_Code;
	}

	public static String extract_Response_Body(String URL) {
		String ResponseBody = given().header("Content-Type", "application/json").when().get(URL).then().extract()
				.response().asString();

		return ResponseBody;
	}
}
