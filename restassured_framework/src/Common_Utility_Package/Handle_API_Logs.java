package Common_Utility_Package;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_API_Logs {

	public static File create_log_dir(String dirname) {
		String currentprojectdir = System.getProperty("user.dir");
		System.out.println(currentprojectdir);
		File log_directory = new File(currentprojectdir + "//Api_Logs//" + dirname);
		delate_dir(log_directory);
		log_directory.mkdir();
		return log_directory;
	}

	public static boolean delate_dir(File Directory) {
		boolean Directory_deleted = Directory.delete();
		if (Directory.exists()) {
			File[] files = Directory.listFiles();
			if (files != null) {
				for (File file : files) {
					if (file.isDirectory()) {
						delate_dir(file);
					} else {
						file.delete();
					}
				}
			}
			Directory_deleted = Directory.delete();
		}
		return Directory_deleted;
	}

	public static void evidence_creator(File dirname, String filename, String endpoint, String requestBody,
			String responseBody) throws IOException {

		// Step 1:- Create the file at given location

		File newfile = new File(dirname + "\\" + filename + ".txt");
		System.out.println("new file created to save evidence:" + newfile.getName());

		// Step 2:- Write data into the file

		FileWriter datawriter = new FileWriter(newfile);
		datawriter.write("End point:" + endpoint + "\n\n");
		datawriter.write("Reuest Body:\n\n" + requestBody + "\n\n");

		datawriter.write("Response Body:\n\n" + responseBody);
		datawriter.close();
		System.out.println("Evidence is written in file:" + newfile.getName());

	}   

}
