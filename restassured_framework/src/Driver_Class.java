
import java.io.IOException;

import Common_Utility_Package.Excel_data_reader;
import TestClass_Package.Delete_TC1;
import TestClass_Package.Post_TC1;
//import TestClass_Package.Get_TC1;
//import TestClass_Package.Put_TC1;
//import TestClass_Package.Patch_TC1;

public class Driver_Class {

	public static void main(String[] args) throws IOException {
		// System.out.println("For Post Method :");

		Post_TC1.executor();

		System.out.println("...............................................................");

		// System.out.println("For Get Method :");
		// Get_TC1.executor();
		System.out.println("................................................................");

		// handle_API_Logs.create_logs_Directory();

		// System.out.println("For Put Method :");
		// Put_TC1.executor();
		System.out.println(".................................................................");

		// System.out.println("For Patch Method :");
		// Patch_TC1.executor();
		System.out.println("...................................................................");

		//Delete_TC1.exeutor();

	}

}
