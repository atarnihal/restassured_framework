package Request_Repository;

import java.io.IOException;
import java.util.ArrayList;

import Common_Utility_Package.Excel_data_reader;

public class Put_Request_Repository extends End_Points {

	public static String Put_TC1_Request() throws IOException {

		ArrayList<String> excelData = Excel_data_reader.Read_Excel_Data("Api_Data.xlsx", "Put_API", "Put_TC3");
		// System.out.println(excelData);
		String req_name = excelData.get(1);
		String req_job = excelData.get(2);
		String requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job
				+ "\",\r\n" + "			                  	+ \"\"\r\n" + "}";

		return requestBody;

	}

}
