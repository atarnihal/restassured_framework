package TestClass_Package;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Method_Package.Trigger_Delete_API_Method;
import Common_Utility_Package.Handle_API_Logs;

public class Delete_TC1 extends Trigger_Delete_API_Method {
@Test
	public static void exeutor() throws IOException {

		//File dirname = Handle_API_Logs.create_log_dir("Delete_TC1");
		String URL = delete_Endpoint();
		for (int i = 0; i < 5; i++) {
			int statusCode = Trigger_Delete_API_Method.extract_SC_Delete(URL);
		System.out.println(statusCode);
			if (statusCode == 204) {
			System.out.println(statusCode);
				//Handle_API_Logs.evidence_creator(dirname,"Delete_TC1", URL,statusCode);
				break;
			} else {
				System.out.println("Desired StatusCode is not found hence retry");
			}
			Assert.assertEquals(statusCode, 201);
		}

	}

}