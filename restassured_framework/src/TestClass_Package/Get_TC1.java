package TestClass_Package;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Method_Package.Trigger_Get_API_Method;
import Common_Utility_Package.Handle_API_Logs;
import io.restassured.path.json.JsonPath;

public class Get_TC1 extends Trigger_Get_API_Method {
	@Test
	public static void executor() throws IOException {
		File dirname = Handle_API_Logs.create_log_dir("Get_TC1");
		int StatusCode = 0;
		for (int i = 0; i < 5; i++) {

			int Status_Code = extract_Status_Code(get_endpoint());
			System.out.println(Status_Code);

			if (Status_Code == 200) {
				String ResponseBody = extract_Response_Body(get_endpoint());
				System.out.println(ResponseBody);
				Handle_API_Logs.evidence_creator(dirname, "Get_TC1", get_endpoint(), Null(), ResponseBody);

				validator(ResponseBody);
				break;
			} else {
				System.out.println("Desired Status Code is invalid hence retry");
			}
			Assert.assertEquals(Status_Code, 200);
		}
	}

	private static String Null() {

		return null;
	}

	public static void validator(String ResponseBody) { // Create an object of ResponseBody

		String[] Exp_id_Array = { "7", "8", "9", "10", "11", "12" };
		String[] Exp_email_Array = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String[] Exp_first_name_Array = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String[] Exp_last_name_Array = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String[] Exp_avtar_Array = { "https://reqres.in/img/faces/7-image.jpg",
				"https://reqres.in/img/faces/8-image.jpg", "https://reqres.in/img/faces/9-image.jpg",
				"https://reqres.in/img/faces/10-image.jpg", "https://reqres.in/img/faces/11-image.jpg",
				"https://reqres.in/img/faces/12-image.jpg" };

		JsonPath jsp_res = new JsonPath(ResponseBody);

		String res_page = jsp_res.getString("page");
		List<Object> res_data = jsp_res.getList("data");
		int count = res_data.size();

		// System.out.println(count);
		// System.out.println(res_data);

		for (int i = 0; i < count; i++) {
			// Array of Id
			String Exp_id = Exp_id_Array[i];
			String res_id = jsp_res.getString("data[" + i + "].id");

			System.out.println(res_id);
			Assert.assertEquals(res_id, Exp_id);
//			// Array of Email
			String Exp_email = Exp_email_Array[i];
			String res_email = jsp_res.getString("data[" + i + "].email");
			System.out.println(res_email);
			Assert.assertEquals(res_email, Exp_email);

			// Array of first name
			String Exp_first_name = Exp_first_name_Array[i];
			String res_first_name = jsp_res.getString("data[" + i + "].first_name");
			System.out.print(res_first_name);
			Assert.assertEquals(res_first_name, Exp_first_name);

			// Array of last name
			String Exp_last_name = Exp_last_name_Array[i];
			String res_last_name = jsp_res.getString("data[" + i + "].last_name");
			System.out.println(res_last_name);
			Assert.assertEquals(res_last_name, Exp_last_name);

			String Exp_avatar = Exp_avtar_Array[i];
			String res_avatar = jsp_res.getString("data[" + i + "].avatar");
			System.out.println(res_avatar);
			Assert.assertEquals(res_avatar, Exp_avatar);
		}

	}

}
