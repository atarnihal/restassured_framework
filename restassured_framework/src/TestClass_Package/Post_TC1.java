package TestClass_Package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import Common_Method_Package.Trigger_API_Method;
import Common_Utility_Package.Handle_API_Logs;
import io.restassured.path.json.JsonPath;

public class Post_TC1 extends Trigger_API_Method {
	private static ExtentReports extent;
	private static ExtentTest test;

	@BeforeSuite
	public void setUp() {
		ExtentSparkReporter spark = new ExtentSparkReporter("./extent-reports/report.html");
		extent = new ExtentReports();
		extent.attachReporter(spark);
	}

	@Test
	public static void executor() throws IOException {
		test = extent.createTest("executor");
		String requestbody = Post_TC1_Request();
		File dirname = Handle_API_Logs.create_log_dir("Post_TC1");
		for (int i = 0; i < 5; i++) {
			int Status_code = extract_status_code(requestbody, post_endpoint());
			System.out.println("Status Code: " + Status_code);
			if (Status_code == 201) {
				String ResponseBody = extract_Response_Body(requestbody, post_endpoint());
				System.out.println("Response Body :" + ResponseBody);
				Handle_API_Logs.evidence_creator(dirname, "post_TC1", post_endpoint(), requestbody, ResponseBody);
				Validator(requestbody, ResponseBody);
				break;
			} else {
				System.out.println("Desired Status Code not found hence, retry");
				test.pass("Test passed");
			}
		}

	}

	@AfterSuite
	public void tearDown() {
		extent.flush();
	}

	// Create the object of Json Path
	public static void Validator(String requestbody, String ResponseBody) throws IOException {

		LocalDateTime CurrentDate = LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0, 11);
		System.out.println(ExpectedDate);

		JsonPath jsp_req = new JsonPath(requestbody);

		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		JsonPath jsp = new JsonPath(ResponseBody);

		String res_name = jsp.getString("name");
		String res_job = jsp.getString("job");
		int res_id = jsp.getInt("id");
		System.out.println("Responsebody id " + res_id);
		String res_createdAt = jsp.getString("createdAt").substring(0, 11);
		System.out.println(res_createdAt);

		// Validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(ExpectedDate, res_createdAt);

	}

}