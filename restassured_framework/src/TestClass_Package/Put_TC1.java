package TestClass_Package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Method_Package.Trigger_Put_API_Method;
import Common_Utility_Package.Handle_API_Logs;
import io.restassured.path.json.JsonPath;

public class Put_TC1 extends Trigger_Put_API_Method {
	@Test
	public static void executor() throws IOException {
		String requestbody = Put_TC1_Request();
		File dirname = Handle_API_Logs.create_log_dir("Put_TC1");
		// retrying request
		for (int i = 0; i < 5; i++) {
			int Status_code = Trigger_Put_API_Method.extract_status_code(requestbody, put_endpoint());
			// int Status_code = extract_status_code(requestbody, put_endpoint());
			System.out.println("Status Code: " + Status_code);
			if (Status_code == 200) {
				String responseBody = Trigger_Put_API_Method.extract_Response_Body(requestbody, put_endpoint());
				System.out.println("Response Body :" + responseBody);
				Handle_API_Logs.evidence_creator(dirname, "Put_TC1", put_endpoint(), Put_TC1_Request(), requestbody);
				Validator(requestbody, responseBody);
				break;
			} else {
				// System.out.println("Desired Status Code not found hence, retry");
			}
		}
	}

	public static void Validator(String requestbody, String responseBody) throws IOException {

		JsonPath jsp_req = new JsonPath(requestbody);

		String req_name = jsp_req.getString("name");
		System.out.println("Requestbody name " + req_name);
		String req_job = jsp_req.getString("job");
		System.out.println("Requestbody job " + req_job);

		JsonPath jsp = new JsonPath(responseBody);

		String res_name = jsp.getString("name");
		System.out.println("Responsebody name " + res_name);
		String res_job = jsp.getString("job");
		System.out.println("Responsebody job " + res_job);
		String res_updatedAt = jsp.getString("updatedAt").substring(0, 11);

		LocalDateTime CurrentDate = LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0, 11);
		System.out.println(ExpectedDate);

		// Validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(ExpectedDate, res_updatedAt);

	}

}
